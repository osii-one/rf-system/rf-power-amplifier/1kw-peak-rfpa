# 1kw peak RFPA | Source Files

## General

The RFPA consists of three PCBs and a casing.
The respective BOMs of these subcomponents contain all parts of amplifier in total – there is no BOM besides these.
Please refer to the `res/img` folder for images of each component. 

## PCBs

The three PCBs are:

- [MRIamp](/src/MRIamp/) (=the main RFPA boarRadio-frequency power amplifierd)
	- project file: `PCB_Project_MRIamp.PrjPCB`
	- board design file: `MRIAMP.PcbDoc`
	- bill of materials: [BOM.csv](https://gitlab.com/osii/rf-system/rf-power-ampli     fier/1kw-peak-rfpa/-/blob/rc/1.0/src/MRIamp/BOM.csv) 
- [TrafoPCB-NoGap](/src/TrafoPCB-NoGap/) (=the PCB for the back of the RF transformer)
	- board design file: `TrafoPCB-NoGap.PcbDoc`
- [TrafoPCB-WithGap](/src/TrafoPCB-WithGap/), (=PCB for the front of the RF transformer)
	- board design file: `TrafoPCB-WithGap.PcbDoc`

The `TrafoPCB-*`-boards are essentially just pieces of FR4 with some holes and one or two copper planes, so they come without BOM and schematics.

If you're looking for Gerber exports: please refer to the respective [release notes](https://gitlab.com/osii/rf-system/rf-power-amplifier/1kw-peak-rfpa/-/releases).


## Enclosure

The folder `enclosure plates` contains the relevant measurements and specifications for each side of the enclosure. 

The files have been created with Schaeffer's [Front Panel Designer](https://www.frontpanelexpress.com/front-panel-designer) and are available in their native proprietary file format.
The software is however (currently) available as Freeware for Windows, macOS and Linux distributions. The software may be used to convert files into another format as needed. Carving the necessary holes in the enclosure by hand is possible and has been done.  


## Diode Cooling Plate

The folder `diode-cooling-plate` contains the relevant measurements for the diode cooling plate. 

The diode cooling plate material is made with aluminum and is shown in [BOM.csv](/src/BOM.csv).


## Capacitor Holder

The folder `capacitor-holder` contains measurements and specifications for each component of the capacitor holder.

## Heatsink

The folder `heatsink` contains the measurements for the holes for each necessary side of the heatsink.

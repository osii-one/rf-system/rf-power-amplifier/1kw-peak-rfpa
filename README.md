# Radio-Frequency Power Amplifier with 1kW peak power

## System specifications

- Class A 12MHz pulse amplifier that can deliver 1kW (60 dBm) pulsed power into a 50Ω load with a maximum duty cycle of 10%. 
- TTL enable signal that controls the gating of the pre-amplifier and the power supply of the power amplifier simultaneously.

For details please see the [RFPA_12MHz-details](/doc/RFPA_12MHz-details.pdf) document.


## System specifications

| Class A 12MHz pulse amplifier | <!-- --> <!-- --> |
| --------------------- | ------------ |
| Max Load Power        | 1kW          |
| Max duty cycle        | 10 $\%$      |
| Max pulse duration    | 20 ms        |
| Bandwidth             | 0.5-12 MHz   |
| Enable signal         | 5V 1 $\%$    |
| Gain @ 4MHz           | 54.3 dB      |
| R<sub>out</sub> @ 4MHz | 48.2 $\Omega$  |
| R<sub>in</sub>         | 50 $\Omega$    |
| Blanking delay        | $\le 500$ ns |


## Repository Overview

The repository follows a Unixish directory structure, inspired by the [Minimal OSH Repository Template](https://gitlab.fabcity.hamburg/software/template-osh-repo-structure-minimal/-/blob/main/mod/unixish/README.md):

- [doc](/doc/): documentation
	- assembly guide
- [res](/res/): additional resources used and referenced throughout the repository
	- image of components 
- [src](/src/): source files
    - bill of materials
    - component design files

## Contributing

### Proprietary Native Files

The design has been created with Altium, files are saved in the native, proprietary format.

### Get in Touch

To join the development, feel free to

- join us on [Matrix](https://matrix.to/#/#osii:matrix.org),
- drop us an [issue](https://gitlab.com/osii/rf-system/rf-power-amplifier/1kw-peak-rfpa/-/issues) or
- file a merge request directly.

## Contributors (alphabetical order)

Original design by Danny de Gans (D.H.deGans@tudelft.nl) from [TU Delft](https://www.tudelft.nl/en/), Netherlands.

## License and Liability

This is an open source hardware project licensed under the CERN Open Hardware Licence Version 2 - Weakly Reciprocal. For more information please check [LICENSE](LICENSE) and [DISCLAIMER](DISCLAIMER.pdf).
